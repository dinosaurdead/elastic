<?php

namespace Rex\Cache\Predis\Exception;

use Psr\Cache\CacheException;

/**
 * Class PredisConnectionException
 * @package Marketplace\Cache\Adapter\Predis\Exception
 */
class PredisConnectionException extends \RuntimeException implements CacheException
{

}
