<?php
namespace Rex\Cache\Elastic;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

/**
 * Class ElasticAdapter
 * @package Rex\Cache\Adapter\Elastic
 */
class ElasticAdapter implements ElasticAdapterInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ElasticConfiguration
     */
    protected $configuration;

    public function __construct(ElasticConfiguration $configuration)
    {
        $this->configuration = $configuration;

        $this->client = ClientBuilder::create()
            ->setHosts($this->configuration->getConfig())
            ->build();

        if (
            $this->configuration->shouldCheckConnection() &&
            !$this->checkConnection()
        ) {
            throw new \Exception('Cant connect to Elastic Server');
        }
    }

    /**
     * @return bool
     */
    public function checkConnection()
    {
        return $this->client->ping();
    }

    /**
     * @param $params
     * @return array
     */
    public function get($params)
    {
        $data = $this->client->search($params);

        $hits = [];
        if(!empty($data['hits']['total']))
            $hits = $data['hits']['hits'][0]['_source'];

        return $hits;
    }

    /**
     * @param $params
     * @return array
     */
    public function getAll($params)
    {
        $data = $this->client->search($params);

        $hits = [];
        foreach ($data['hits']['hits'] as $hit) {
            $hits[] = $hit['_source'];
        }

        return $hits;
    }

    /**
     * @param $params
     * @return array
     */
    public function getAllWithTotal($params)
    {
        $data = $this->client->search($params);

        $hits = [];
        foreach ($data['hits']['hits'] as $hit) {
            $hits[] = $hit['_source'];
        }

        $hits['total'] = $data['hits']['total'];

        return $hits;
    }

    /**
     * @param $params
     */
    public function save($params)
    {
        $this->client->index($params);
    }
}
