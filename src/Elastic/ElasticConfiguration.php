<?php
namespace Rex\Cache\Elastic;

/**
 * Class ElasticConfiguration
 * @package Adapters\Elastic
 */
class ElasticConfiguration
{
    public function __construct(array $configuration)
    {
        $properties = get_object_vars($this);
        $configurationKeys = array_keys($configuration);

        foreach ($properties as $property => $value) {
            if (in_array($property, $configurationKeys)) {
                $this->$property =  $configuration[$property];
            }
        }
    }

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $port;

    /**
     * @var
     */
    protected $shouldCheckConnection;

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return ElasticConfiguration
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     * @return ElasticConfiguration
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return bool
     */
    public function shouldCheckConnection()
    {
        return $this->shouldCheckConnection;
    }

    public function getConfig()
    {
        return [
            $this->getHost() . ':' . $this->getPort()
        ];
    }
}
