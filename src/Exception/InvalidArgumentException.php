<?php

namespace Rex\Cache\Exception;

use Psr\Cache\InvalidArgumentException as InvalidArgumentExceptionInterface;

/**
 * Class InvalidArgumentException
 * @package Cache\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements InvalidArgumentExceptionInterface
{
}
