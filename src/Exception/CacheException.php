<?php

namespace Rex\Cache\Exception;

use Psr\Cache\CacheException as CacheExceptionInterface;

/**
 * Class CacheException
 * @package Cache\Exception
 */
class CacheException extends \InvalidArgumentException implements CacheExceptionInterface
{
}
