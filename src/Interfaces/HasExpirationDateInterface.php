<?php

namespace Rex\Cache\Interfaces;

/**
 * Interface HasExpirationDateInterface
 * @package Marketplace\Cache\Interfaces
 */
interface HasExpirationDateInterface
{
    /**
     * The date and time when the object expires.
     *
     * @return \DateTime|null
     */
    public function getExpirationDate();
}
